package Widerstandsnetze;

/**
 * Diese Klasse repräsentiert Widerstandsnetze, die selbst aus zwei
 * Widerstandsnetzen zusammengebaut sind.
 * 
 * @author Julia,Chris
 *
 */
abstract class AbstractZusammengesetzesWiderstandsnetz
	extends AbstractWiderstandsnetz
{
    protected Widerstandnetz _erstesNetz;
    protected Widerstandnetz _zweitesNetz;

    /**
     * Initialisiert ein Widerstandsnetz aus zwei Widerstandsnetzen.
     * 
     * @param erstesNetz
     * @param zweitesNetz
     * @require erstesNetz!=null && zweitesNetz!=null
     */
    public AbstractZusammengesetzesWiderstandsnetz(
	   Widerstandnetz erstesNetz,
	   Widerstandnetz zweitesNetz)
    {
	assert erstesNetz != null : "Vorbedingung verletzt: erstesNetz != null";
	assert zweitesNetz != null : "Vorbedingung verletzt: zweitesNetz != null";
	_erstesNetz = erstesNetz;
	_zweitesNetz = zweitesNetz;

    }

}

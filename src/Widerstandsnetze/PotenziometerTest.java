package Widerstandsnetze;

import static org.junit.Assert.*;

import org.junit.Test;

public class PotenziometerTest 
{

	@Test
	/**
	 * Testet, ob sich die Widerstandszahl des Potenziometers korrekt ändern
	 * lässt.
	 */
	public void testSetOhm()
	{
		Potenziometer potenzi = new Potenziometer(400);
		potenzi.setOhm(200);
		assertEquals(potenzi.getOhm(), 200,0);
		potenzi.setOhm(11);
		assertEquals(potenzi.getOhm(), 11,0);
		
	}

	@Test
	/**
	 * Testet, ob das Potenziometer die korrekte Anzahl Widerstände zurückgibt.
	 */
	public void testGetAnzahlWiderstaende() 
	{
		Potenziometer widerstand = new Potenziometer(500);
		assertEquals(widerstand.getAnzahlWiderstaende(), 1);
		Potenziometer widerstand2 = new Potenziometer(435);
		assertEquals(widerstand2.getAnzahlWiderstaende(),1 );
	}

	@Test
	/**
	 * Testet, ob das Potenziometer die korrekt Ohmzahl zurückgibt.
	 */
	public void testGetOhm() 
	{
		Potenziometer potenzi = new Potenziometer(400);
		assertEquals(potenzi.getOhm(), 400,0);
		Potenziometer tenzi = new Potenziometer(3350);
		assertEquals(tenzi.getOhm(), 3350,0);
		
	}

}

package Widerstandsnetze;

import static org.junit.Assert.*;

import org.junit.Test;

public class WiderstandTest {


	@Test
	public void testGetOhm() 
	{
		Widerstand widerstand = new Widerstand(500);
		assertEquals(widerstand.getOhm(),500,0);
		Widerstand widerstand2= new Widerstand(0);
		assertEquals(widerstand2.getOhm(), 0,0);
	}

	
	@Test
	public void testGetAnzahlWiderstaende() 
	{
		Widerstand widerstand = new Widerstand(500);
		assertEquals(widerstand.getAnzahlWiderstaende(), 1);
		Widerstand widerstand2 = new Widerstand(435);
		assertEquals(widerstand2.getAnzahlWiderstaende(),1 );
	}

}

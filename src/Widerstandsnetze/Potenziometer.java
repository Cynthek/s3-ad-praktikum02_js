package Widerstandsnetze;
/**
 * Potenziometer sind einfache
 * Widerstände mit einem regelbaren Widerstandswert. 
 * @author Julia, Chris
 *
 */
public class Potenziometer extends Widerstand
{

	public Potenziometer(double ohm) 
	{
		super(ohm);
	}
	public void setOhm(int neuerWiderstand)
	{
		_gesamtWiderstand= neuerWiderstand;
	}

}

package Widerstandsnetze;

import static org.junit.Assert.*;

import org.junit.Test;

public class SeriellesWiderstandsnetzTest
{
    @Test
    /**
     * Testet, ob ein Netz aus zwei Einzelwiderständen die richtige
     * Widerstandsanzahl zurückgibt.
     */
    public void testAnzahlEinfachZusammengesetzt()
    {
	SeriellesWiderstandsnetz testNetz = new SeriellesWiderstandsnetz(
		new Widerstand(400), new Widerstand(135));
	assertEquals(testNetz.getAnzahlWiderstaende(), 2);
    }

    @Test
    /**
     * Testet, ob ein Netz aus zwei zusammengesetzten Widerstandsnetzen die
     * richtige Widerstandsanzahl zurückgibt.
     */
    public void testAnzahlMehrfachZusammengesetzt()
    {
	SeriellesWiderstandsnetz netz1 = new SeriellesWiderstandsnetz(
		new Widerstand(400), new Widerstand(135));
	SeriellesWiderstandsnetz netz2 = new SeriellesWiderstandsnetz(
		new Widerstand(345), new Widerstand(746));
	SeriellesWiderstandsnetz zusammengesetzt = new SeriellesWiderstandsnetz(
		netz1, netz2);
	assertEquals(zusammengesetzt.getAnzahlWiderstaende(), 4);
    }

    @Test
    /**
     * Testet, ob ein Netz aus vielen Einzelwiderständen die richtige
     * Widerstandsanzahl zurückgibt.
     */
    public void testAnzahlVielfachZusammengesetzt()
    {
	SeriellesWiderstandsnetz netz1 = new SeriellesWiderstandsnetz(
		new Widerstand(400), new Widerstand(135));
	SeriellesWiderstandsnetz netz2 = new SeriellesWiderstandsnetz(
		new Widerstand(345), new Widerstand(746));
	SeriellesWiderstandsnetz zusammengesetzt = new SeriellesWiderstandsnetz(
		netz1, netz2);
	SeriellesWiderstandsnetz netz3 = new SeriellesWiderstandsnetz(
		new Widerstand(123), new Widerstand(15));
	SeriellesWiderstandsnetz netz4 = new SeriellesWiderstandsnetz(
		new Widerstand(98), new Widerstand(43));
	SeriellesWiderstandsnetz zusammengesetzt2 = new SeriellesWiderstandsnetz(
		netz3, netz4);
	SeriellesWiderstandsnetz testnetz = new SeriellesWiderstandsnetz(
		zusammengesetzt, zusammengesetzt2);
	assertEquals(testnetz.getAnzahlWiderstaende(), 8);
    }

    @Test
    /**
     * Testet, ob ein Netz aus zwei Einzelwiderständen die richtige Ohmzahl
     * zurückgibt.
     */
    public void testGetOhmEinfachesNetz()
    {
	SeriellesWiderstandsnetz testNetz = new SeriellesWiderstandsnetz(
		new Widerstand(400), new Widerstand(200));
	assertEquals(testNetz.getOhm(), 600, 0.1);

    }

    @Test
    /**
     * Testet, ob ein aus mehreren Netzen zusammengesetztes Netz die richtige
     * Ohmzahl zurückgibt.
     */
    public void testGetOhmMehrfachNetz()
    {
	SeriellesWiderstandsnetz testNetzaus2Parallelen = new SeriellesWiderstandsnetz(
		new SeriellesWiderstandsnetz(new Widerstand(100),
			new Widerstand(200)),
		new SeriellesWiderstandsnetz(new Widerstand(300),
			new Widerstand(400)));
	assertEquals(testNetzaus2Parallelen.getOhm(), 1000, 0.1);
    }

}

package Widerstandsnetze;

/**
 * Diese Klasse repräsentiert einfach Widerstände.
 * 
 * @author Julia,Chris
 *
 */
public class Widerstand extends AbstractWiderstandsnetz
	implements Widerstandnetz
{

    /**
     * Initialisiert ein Widerstandsnetz mit einem unveränderlichen
     * Widerstandswert.
     * 
     * @param ohm Der Widerstandswert
     */
    public Widerstand(double ohm)
    {
	_gesamtWiderstand = ohm;
    }

    @Override
    public int getAnzahlWiderstaende()
    {
	return 1;
    }

}

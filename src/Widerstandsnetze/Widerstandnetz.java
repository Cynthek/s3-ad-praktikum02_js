package Widerstandsnetze;

public interface Widerstandnetz 
{
	 /**
	  * Liefert den Gesamtwiderstand des Netzes, dieser ist immer mindestens 0.
	  */
	 double getOhm();
	
	/**
	 *  Liefert die Anzahl an einfachen Widerständen im Netz 
	 */
	 int getAnzahlWiderstaende(); 

}

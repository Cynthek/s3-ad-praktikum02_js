package Widerstandsnetze;

import static org.junit.Assert.*;

import org.junit.Test;

public class ParallelesWiderstandsnetzTest
{

    @Test
    /**
     * Testet, ob ein Netz aus zwei Einzelwiderständen die richtige
     * Widerstandsanzahl zurückgibt.
     */
    public void testAnzahlEinfachZusammengesetzt()
    {
	ParallelesWiderstandsnetz testNetz = new ParallelesWiderstandsnetz(
		new Widerstand(400), new Widerstand(135));
	assertEquals(testNetz.getAnzahlWiderstaende(), 2);
    }

    @Test
    /**
     * Testet, ob ein Netz aus zwei zusammengesetzten Widerstandsnetzen die
     * richtige Widerstandsanzahl zurückgibt.
     */
    public void testAnzahlMehrfachZusammengesetzt()
    {
	ParallelesWiderstandsnetz netz1 = new ParallelesWiderstandsnetz(
		new Widerstand(400), new Widerstand(135));
	ParallelesWiderstandsnetz netz2 = new ParallelesWiderstandsnetz(
		new Widerstand(345), new Widerstand(746));
	ParallelesWiderstandsnetz zusammengesetzt = new ParallelesWiderstandsnetz(
		netz1, netz2);
	assertEquals(zusammengesetzt.getAnzahlWiderstaende(), 4);
    }

    @Test
    /**
     * Testet, ob ein Netz aus vielen Einzelwiderständen die richtige
     * Widerstandsanzahl zurückgibt.
     */
    public void testAnzahlVielfachZusammengesetzt()
    {
	ParallelesWiderstandsnetz netz1 = new ParallelesWiderstandsnetz(
		new Widerstand(400), new Widerstand(135));
	ParallelesWiderstandsnetz netz2 = new ParallelesWiderstandsnetz(
		new Widerstand(345), new Widerstand(746));
	ParallelesWiderstandsnetz zusammengesetzt = new ParallelesWiderstandsnetz(
		netz1, netz2);
	ParallelesWiderstandsnetz netz3 = new ParallelesWiderstandsnetz(
		new Widerstand(123), new Widerstand(15));
	ParallelesWiderstandsnetz netz4 = new ParallelesWiderstandsnetz(
		new Widerstand(98), new Widerstand(43));
	ParallelesWiderstandsnetz zusammengesetzt2 = new ParallelesWiderstandsnetz(
		netz3, netz4);
	ParallelesWiderstandsnetz testnetz = new ParallelesWiderstandsnetz(
		zusammengesetzt, zusammengesetzt2);
	assertEquals(testnetz.getAnzahlWiderstaende(), 8);
    }

    @Test
    /**
     * Testet, ob ein Netz aus zwei Einzelwiderständen die richtige Ohmzahl
     * zurückgibt.
     */
    public void testGetOhmEinfachesNetz()
    {
	final double parallelOhm = 133.33333333333333333;
		epsilon = 0.1;
	
	ParallelesWiderstandsnetz testNetz = new ParallelesWiderstandsnetz(
		new Widerstand(400), new Widerstand(200));
	assertEquals(parallelOhm, testNetz.getOhm(), epsilon);

    }

    @Test
    /**
     * Testet, ob ein aus mehreren Netzen zusammengesetztes Netz die richtige
     * Ohmzahl zurückgibt.
     */
    public void testGetOhmMehrfachNetz()
    {
	ParallelesWiderstandsnetz testNetzaus2Parallelen = new ParallelesWiderstandsnetz(
		new ParallelesWiderstandsnetz(new Widerstand(100),
			new Widerstand(200)),
		new ParallelesWiderstandsnetz(new Widerstand(300),
			new Widerstand(400)));
	assertEquals(testNetzaus2Parallelen.getOhm(), 48, 0.1);
    }

}

package Widerstandsnetze;

abstract class AbstractWiderstandsnetz implements Widerstandnetz
{
    protected double _gesamtWiderstand;
    protected int _widerstandsAnzahl;

    /**
     * Liefert den Gesamtwiderstand des Netzes, dieser ist immer mindestens 0.
     * 
     * @ensure result >= 0
     */
    public double getOhm()
    {
	assert _gesamtWiderstand >= 0 : "Vorbedingung verletzt: _gesamtWiderstand>=0";
	return _gesamtWiderstand;
    }

    /**
     * Liefert die Anzahl an einfachen Widerständen im Netz
     * @ensure result >=1
     */
    public int getAnzahlWiderstaende()
    {
	assert _widerstandsAnzahl>=1 : "Vorbedingung verletzt: _widerstandsAnzahl>=1";
	return _widerstandsAnzahl;
    }

}

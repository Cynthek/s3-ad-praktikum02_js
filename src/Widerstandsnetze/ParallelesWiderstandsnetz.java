package Widerstandsnetze;

/**
 * Diese Klasse repräsentiert Widerstandsnetze, die selbst aus zwei
 * Parallel-Schaltungen zusammengesetzt sind. 1/R = 1/R1 + 1/R2 also gilt:
 *  R = 1/(1/R1 + 1/R2)
 * 
 * @author Julia,Chris
 *
 */
public class ParallelesWiderstandsnetz
	extends AbstractZusammengesetzesWiderstandsnetz
{
    /**
     * Initialisiert ein Widerstandsnetz, das aus zwei parallel geschalteten
     * Einzelwiderständen oder Widerstandsnetzen zusammengesetzt ist.
     * @param erstesNetz
     * @param zweitesNetz
     */
    public ParallelesWiderstandsnetz(AbstractWiderstandsnetz erstesNetz,
	    AbstractWiderstandsnetz zweitesNetz)
    {
	super(erstesNetz, zweitesNetz);
	_gesamtWiderstand = 1
		/ (1 / _erstesNetz.getOhm() + 1 / _zweitesNetz.getOhm());
	_widerstandsAnzahl = _erstesNetz.getAnzahlWiderstaende()
		+ _zweitesNetz.getAnzahlWiderstaende();
    }
    @Override
    public double getOhm()
    {
	_gesamtWiderstand = 1
		/ (1 / _erstesNetz.getOhm() + 1 / _zweitesNetz.getOhm());
	_widerstandsAnzahl = _erstesNetz.getAnzahlWiderstaende()
		+ _zweitesNetz.getAnzahlWiderstaende();
	return _gesamtWiderstand;
    }
}

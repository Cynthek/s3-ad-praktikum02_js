package Widerstandsnetze.startup;

import Widerstandsnetze.ParallelesWiderstandsnetz;
import Widerstandsnetze.Potenziometer;
import Widerstandsnetze.SeriellesWiderstandsnetz;
import Widerstandsnetze.Widerstand;

public class startupMitPotenziometer 
{

	public static void main(String[] args) 
	{
		Widerstand ohm100= new Widerstand(100);
		Widerstand ohm300=new Widerstand(300);
		ParallelesWiderstandsnetz parallel= new ParallelesWiderstandsnetz(ohm100, ohm300);
		
		Widerstand ohm200= new Widerstand(200);
		SeriellesWiderstandsnetz serieller= new SeriellesWiderstandsnetz(parallel, ohm200);
		
		Potenziometer potenzi=new Potenziometer(0);
		Widerstand ohm500= new Widerstand(500);
		SeriellesWiderstandsnetz seriell= new SeriellesWiderstandsnetz(potenzi, ohm500);
		ParallelesWiderstandsnetz parallel2= new ParallelesWiderstandsnetz(seriell, serieller);
		
		Widerstand ohm600= new Widerstand(600);
		ParallelesWiderstandsnetz gesamtesNetz= new ParallelesWiderstandsnetz(parallel2,ohm600);
		for (int i = 0; i<=5000; i=i+200)
		{
		    
			System.out.println(gesamtesNetz.getOhm());
			 potenzi.setOhm(i);
		}

	}
 

}
 
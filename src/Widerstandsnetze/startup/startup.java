package Widerstandsnetze.startup;

import Widerstandsnetze.ParallelesWiderstandsnetz;
import Widerstandsnetze.SeriellesWiderstandsnetz;
import Widerstandsnetze.Widerstand;

public class startup 
{

	public static void main(String[] args) 
	{
		Widerstand ohm100= new Widerstand(100);
		Widerstand ohm300=new Widerstand(300);
		ParallelesWiderstandsnetz parallel= new ParallelesWiderstandsnetz(ohm100, ohm300);
		
		Widerstand ohm200= new Widerstand(200);
		SeriellesWiderstandsnetz serieller= new SeriellesWiderstandsnetz(parallel, ohm200);
		
		Widerstand ohm400=new Widerstand(400);
		Widerstand ohm500= new Widerstand(500);
		SeriellesWiderstandsnetz seriell= new SeriellesWiderstandsnetz(ohm400, ohm500);
		ParallelesWiderstandsnetz parallel2= new ParallelesWiderstandsnetz(seriell, serieller);
		
		Widerstand ohm600= new Widerstand(600);
		ParallelesWiderstandsnetz gesamtesNetz= new ParallelesWiderstandsnetz(parallel2,ohm600);
		System.out.println(gesamtesNetz.getOhm() +"-"+ gesamtesNetz.getAnzahlWiderstaende());

	}

}
 
package Widerstandsnetze;

/**
 * Diese Klasse repräsentiert Widerstandsnetze, die selbst aus zwei
 * Reihen-Schaltungen zusammengesetzt sind. R = R1 + R2
 * 
 * @author Julia,Chris
 *
 */
public class SeriellesWiderstandsnetz
	extends AbstractZusammengesetzesWiderstandsnetz
{
    /**
     * Erstellt ein zusammengesetzes Netz aus in Reihe geschalteten Einzelwiderständen
     * oder Widerstandsnetzen.
     * @param erstesNetz
     * @param zweitesNetz
     */
    public SeriellesWiderstandsnetz(AbstractWiderstandsnetz erstesNetz,
	    AbstractWiderstandsnetz zweitesNetz)
    {
	super(erstesNetz, zweitesNetz);
	_gesamtWiderstand = _erstesNetz.getOhm() + _zweitesNetz.getOhm();
	_widerstandsAnzahl = _erstesNetz.getAnzahlWiderstaende()
		+ _zweitesNetz.getAnzahlWiderstaende();
    }
    @Override
    public double getOhm()
    {
	_gesamtWiderstand = _erstesNetz.getOhm() + _zweitesNetz.getOhm();
	_widerstandsAnzahl = _erstesNetz.getAnzahlWiderstaende()
		+ _zweitesNetz.getAnzahlWiderstaende();
	return _gesamtWiderstand;
    }
}

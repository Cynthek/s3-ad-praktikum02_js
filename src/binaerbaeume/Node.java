package binaerbaeume;

import java.util.LinkedList;
import java.util.Queue;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Exemplare dieser Klasse repräsentieren Knoten eines Baumes.
 * 
 * @author Julia,Chris
 *
 */
public class Node
{
    private int _value;
    private Node _leftChild;
    private Node _rightChild;

    /**
     * Initialisiert einen Knoten ohne Kindknoten.
     * 
     * @param value
     *            Der Wert des Knotens.
     */
    public Node(int value)
    {
	_value = value;
    }

    /**
     * Initialisiert einen Knoten mit einem linken Kindknoten.
     * 
     * @require leftChild!=null
     */
    public Node(int value, @NonNull Node leftChild)
    {

	_value = value;
	_leftChild = leftChild;
    }

    /**
     * Initialisiert einen Knoten mit zwei Kindknoten.
     * 
     * @param value
     * @param leftChild
     * @param rightChild
     * @require leftChild!= null, rightChild!=null
     */
    public Node(int value, Node leftChild, Node rightChild)
    {
	assert rightChild != null : "Vorbedingung verletzt: rightChild != null";
	assert leftChild != null : "Vorbedingung verletzt: leftChild != null";
	_value = value;
	_leftChild = leftChild;
	_rightChild = rightChild;
    }

    /**
     * Liefert den linken Kindknoten.
     * 
     * @return der linke Kindknoten
     */
    public Node getLeftChild()
    {
	return _leftChild;
    }

    /**
     * Liefert den rechten Kindknoten.
     * 
     * @return der rechte Kindknoten
     */
    public Node getRightChild()
    {
	return _rightChild;
    }

    /**
     * Liefert den im Knoten gespeicherten Wert.
     */
    public int getValue()
    {
	return _value;
    }

    /**
     * Gibt die Summe der Werte aus Knoten und seinen Kindknoten zurück.
     */
    public int getSum()
    {
	int summe = this.getValue();
	if (_leftChild != null)
	{
	    summe = summe + _leftChild.getSum();
	}
	if (_rightChild != null)
	{
	    summe = summe + _rightChild.getSum();
	}
	return summe;
    }

    /**
     * Gibt die Anzahl aller Kindknoten inkl. sich selbst zurück.
     * 
     * @return Anzahl aller Kindknoten
     */
    public int getNodeCount()
    {
	int result = 1;
	if (_leftChild != null)
	{
	    result = result + _leftChild.getNodeCount();
	}
	if (_rightChild != null)
	{
	    result = result + _rightChild.getNodeCount();
	}
	return result;
    }

    public String print()
    {
	Node n = this;
	Queue<Node> q = new LinkedList<>();
	q.add(n);
	String result = "";
	while (q.size() > 0)
	{
	    if (n._leftChild != null)
	    {
		q.add(n._leftChild);
	    }
	    if (n._rightChild != null)
	    {
		q.add(n._rightChild);
	    }
	   result = result + "-" + q.poll().getValue();
	    n= q.peek();
	}
	return result;
    }
    @Override
    public boolean equals(Object object)
    {
	boolean result = true;
	if(object instanceof Node)
	{
	    Node o= (Node) object;
	    if (o._value!= _value)
	    {
		result = false;
	    }
	    if(o.getLeftChild()!=null)
	    {
		result = o.getLeftChild().equals(this.getLeftChild());
		
	    }
	    if (o.getRightChild()!=null)
	    {
		result = o.getRightChild().equals(this.getRightChild());
	    }
	}
	   return result;
    }

}

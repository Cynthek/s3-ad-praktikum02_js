package binaerbaeume;

public interface BinaryTree
{
    /**
     * Gibt den im Wurzelknoten gehaltenen int-Wert zurück.
     */
    int getValue();

    /**
     * Eine sondierende Operation, gibt den gesamten linken Teilbaum zurück.
     */
    BinaryTree getLeftTree();

    /**
     * Eine sondierende Operation, gibt den gesamten rechten Teilbaum zurück.
     */
    BinaryTree getRightTree();
    
    /**
     * Gibt die Summe aller Werte im Baum zurück.
     */
    int getSum();
    /**
     * Liefert den Wurzelknoten.
     */
    Node getRoot();
    
    /**
     * Gibt die Anzahl aller Knoten im Baum zurück.
     */
    int getSize();
    /**
     * Test auf Gleichheit.
     */
  
    boolean equals(Object o);
    int hashCode();
    
    
}

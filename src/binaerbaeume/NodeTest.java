package binaerbaeume;

import static org.junit.Assert.*;

import org.junit.Test;

public class NodeTest
{

    @Test
    public void testgetValue()
    {
	Node testNode= new Node(21);
	assertEquals(21, testNode.getValue());
    }
    
    @Test
    public void testGetSum()
    {
	Node testNode1= new Node(21);
	Node testNode2=new Node(14);
	Node testNode3=new Node(2,testNode1,testNode2);
	assertEquals(37, testNode3.getSum());
	assertEquals(21, testNode1.getSum());
    }
    
    @Test
    public void testGetNodeCount()
    {
	Node testNode1= new Node(21);
	Node testNode2=new Node(14);
	Node testNode3=new Node(2,testNode1,testNode2);
	assertEquals(3, testNode3.getNodeCount());
	assertEquals(1, testNode1.getNodeCount());
    }
    @Test
    public void testEquals()
    {
	Node testNode1 = new Node(21);
	Node testNode2=new Node(21);
	assertEquals(testNode1, testNode2);
	
	Node testNodeX=new Node(34);
	Node testNode3=new Node(2,testNode1,testNodeX);
	Node testNode4= new Node(2, new Node(21),new Node(34));
	
	
    }
    

}

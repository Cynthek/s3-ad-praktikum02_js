package binaerbaeume;

import org.eclipse.jdt.annotation.NonNull;

public class LinkedBinaryTree implements BinaryTree
{

    private Node _root;

    /**
     * Dieser Konstruktor initialisiert einen neuen Baum, so dass er nur aus
     * einem Wurzelknoten mit dem gegebenen Wert besteht.
     * 
     * @param value
     *            Der Wert des Knotens.
     */
    public LinkedBinaryTree(int value)
    {
	_root = new Node(value);
    }

    /**
     * Initialisert einen Baum mit einem Wurzelknoten, der den übergebenen
     * int-Wert hält und den an dem der übergebene linke Binärbaum hängt.
     * 
     * @param value Der Wert des neuen Wurzelknoten
     * @param leftTree Der linke Teilbaum
     * @require leftTree!=null
     */
    public LinkedBinaryTree(int value,@NonNull LinkedBinaryTree leftTree)
    {
	_root = new Node(value, leftTree.getRoot());
    }

    /**
     * Initialisert einen Baum mit einem Wurzelknoten, der den übergebenen
     * int-Wert hält und den an dem der übergebene linke und rechte Binärbaum
     * hängen.
     * 
     * @param value Der Wert des neuen Wurzelknoten
     * @param leftTree!= null, rightTree!=null
     */
    public LinkedBinaryTree(int value, LinkedBinaryTree leftTree,
	    LinkedBinaryTree rightTree)
    {
	_root = new Node(value, leftTree.getRoot(), rightTree.getRoot());
    }
    
    /**
     * Liefert den Wurzelknoten.
     * @return Der Wurzelknoten Node
     */
    public Node getRoot()
    {

	return _root;
    }

   /**
    * Liefert den im Wurzelknoten gehaltenen Wert.
    */
    public int getValue()
    {

	return _root.getValue();
    }

    
    /**
     * Liefert den gesamten linken Teilbaum. 
     * @require _root.getLeftChild()!=null
     */
    public LinkedBinaryTree getLeftTree()
    {	
	assert _root.getLeftChild() != null : "Vorbedingung verletzt: _root.getLeftChild() != null";

	LinkedBinaryTree leftTree = new LinkedBinaryTree(
		_root.getLeftChild().getValue());
	leftTree._root = _root.getLeftChild();
	return leftTree;
    }

    /**
     * Liefert den gesamten rechten Teilbaum.
     * @require _root.getRightChild()!=null
     */
    public LinkedBinaryTree getRightTree()
    {
	assert _root.getRightChild() != null : "Vorbedingung verletzt: _root.getRightChild() != null";
	LinkedBinaryTree rightTree = new LinkedBinaryTree(
		_root.getRightChild().getValue());
	rightTree._root = _root.getRightChild();
	return rightTree;
    }

    /**
     * Liefert die Summe der Werte aller Knoten.
     */
    public int getSum()
    {
	return _root.getSum();
    }
    /**
     * Liefert die Anzahl aller im Baum enthaltenen Knoten.
     */
    public int getSize()
    {
	return _root.getNodeCount();
    }

    @Override
    /**
     * The == operator always compares identity.
     */
    public boolean equals(Object o)
    {
	boolean result = false;
	if (o instanceof LinkedBinaryTree)
	
	    result = true;
	    LinkedBinaryTree object = (LinkedBinaryTree) o;
	    if (object.getValue() != getValue())
	    {
		return false;
	    }
	    if (object.getSize() != getSize())
	    {
		return false;
	    }
	    if (object.getSum() != getSum())
	    {
		return false;
	    }
	    if (_root.getLeftChild()!=null)
	    {
		result = object._root.getLeftChild().equals(this._root.getLeftChild());
	    }
	    if (_root.getRightChild()!=null)
	    {
		result = object._root.getRightChild().equals(this._root.getRightChild());
	    }
	
	return result;
    }
    @Override
    public int hashCode()
    {
	int result = 5;
	result = result* getSize()*31;
	result= result* getSum()+17;
	result= result*getValue()*5;
	return result;
    }

     public String print()
     {
	 return _root.print();
     }
}

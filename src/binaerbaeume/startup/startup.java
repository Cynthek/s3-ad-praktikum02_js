package binaerbaeume.startup;

import binaerbaeume.LinkedBinaryTree;

public class startup 
{

	public static void main(String[] args)
	{
		System.out.println(erstellebeispielbaum().getSum());
		System.out.println(erstellebeispielbaum().getSize());
		System.out.println(erstellebeispielbaum().getRoot().print());
	}
	
	private static LinkedBinaryTree erstellebeispielbaum()
	{
		
		LinkedBinaryTree links1Stufe1= new LinkedBinaryTree(0);
		LinkedBinaryTree links2Stufe1= new LinkedBinaryTree(9);
		LinkedBinaryTree linksStufe2 = new LinkedBinaryTree(3, links1Stufe1,links2Stufe1);
		
		LinkedBinaryTree rechts1Stufe1= new LinkedBinaryTree(4);
		LinkedBinaryTree rechts2Stufe1= new LinkedBinaryTree(7);
		LinkedBinaryTree rechtsStufe2 = new LinkedBinaryTree(2, rechts1Stufe1,rechts2Stufe1);
		
		LinkedBinaryTree beispielbaum=new LinkedBinaryTree(67,linksStufe2,rechtsStufe2);
		return beispielbaum;
		
	}

}

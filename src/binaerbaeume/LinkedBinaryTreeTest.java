package binaerbaeume;

import static org.junit.Assert.*;

import org.junit.Test;

public class LinkedBinaryTreeTest
{

    @Test
    public void testKonstruktorLinkedBinaryTreeInt()
    {
	LinkedBinaryTree testTree= new LinkedBinaryTree(20);
	assertEquals(20, testTree.getValue());
	
	LinkedBinaryTree testTreeNegativ= new LinkedBinaryTree(-10);
	assertEquals(-10, testTreeNegativ.getValue());
	
	LinkedBinaryTree testTreeGroß= new LinkedBinaryTree(28370);
	assertEquals(28370, testTreeGroß.getValue());	
	
    }

    @Test
    //TODO
    public void testGetLeftTree()
    {
	LinkedBinaryTree linkerTree= new LinkedBinaryTree(20);
	LinkedBinaryTree testTree= new LinkedBinaryTree(30, linkerTree);
	assertEquals(linkerTree, testTree.getLeftTree());
	
	LinkedBinaryTree großerTree= new LinkedBinaryTree(10, linkerTree);
	LinkedBinaryTree großerTestTree= new LinkedBinaryTree(15, großerTree);
	assertEquals(großerTree, großerTestTree.getLeftTree());
    }
    
    @Test
    public void testGetRightTree()
    {
	LinkedBinaryTree linkerTree= new LinkedBinaryTree(50);
	LinkedBinaryTree rechterTree= new LinkedBinaryTree(20);
	LinkedBinaryTree kleinertestTree= new LinkedBinaryTree(30, linkerTree,rechterTree);
	assertEquals(rechterTree, kleinertestTree.getRightTree());
	
	LinkedBinaryTree zweiterLinkerTree= new LinkedBinaryTree(10);
	LinkedBinaryTree großerTree= new LinkedBinaryTree(10, zweiterLinkerTree,kleinertestTree);
	LinkedBinaryTree dritterLinkerTree= new LinkedBinaryTree(40);
	LinkedBinaryTree großerTestTree= new LinkedBinaryTree(15, dritterLinkerTree,großerTree);
	assertEquals(großerTree, großerTestTree.getRightTree());
    }


    @Test
    public void testGetValue()
    {

	LinkedBinaryTree linkerTree= new LinkedBinaryTree(20);
	assertEquals(20, linkerTree.getValue());
	
	LinkedBinaryTree testTree= new LinkedBinaryTree(30, linkerTree);
	assertEquals(30, testTree.getValue());
	
	LinkedBinaryTree rechterTree=new LinkedBinaryTree(40);
	LinkedBinaryTree großerTestTree= new LinkedBinaryTree(50, linkerTree,rechterTree);
	assertEquals(50, großerTestTree.getValue());
	
    }


    @Test
    public void testGetSum()
    {
	LinkedBinaryTree linkerTree= new LinkedBinaryTree(20);
	assertEquals(20, linkerTree.getSum());
	
	LinkedBinaryTree testTree= new LinkedBinaryTree(30, linkerTree);
	assertEquals(50, testTree.getSum());
	
	LinkedBinaryTree rechterTree=new LinkedBinaryTree(40);
	LinkedBinaryTree großerTestTree= new LinkedBinaryTree(50, testTree,rechterTree);
	assertEquals(140, großerTestTree.getSum());
    }

    @Test
    public void testGetSize()
    {
	LinkedBinaryTree linkerTree=new LinkedBinaryTree(10);
	assertEquals(1, linkerTree.getSize());
	
	LinkedBinaryTree zusammengesetzt=new LinkedBinaryTree(20,linkerTree);
	assertEquals(2, zusammengesetzt.getSize());
	
	LinkedBinaryTree rechterTree= new LinkedBinaryTree(30);
	LinkedBinaryTree großerTree=new LinkedBinaryTree(40,zusammengesetzt,rechterTree);
	assertEquals(4,großerTree.getSize());
    }
    
    @Test
    public void testEquals()
    {
	LinkedBinaryTree lbt= new LinkedBinaryTree(5);
	LinkedBinaryTree lbt2= new LinkedBinaryTree(5);
	assertEquals(lbt, lbt2);
	
	LinkedBinaryTree zusammengesetzt= new LinkedBinaryTree(10,lbt);
	LinkedBinaryTree zusammengesetzt2= new LinkedBinaryTree(10, lbt2);
	assertEquals(zusammengesetzt, zusammengesetzt2);

    }
    
    @Test 
    public void testHashCode()
    {
	LinkedBinaryTree lbt= new LinkedBinaryTree(5);
	LinkedBinaryTree lbt2= new LinkedBinaryTree(5);
	assertTrue(lbt.hashCode()==lbt2.hashCode());
	
	LinkedBinaryTree zusammengesetzt= new LinkedBinaryTree(10,lbt);
	LinkedBinaryTree zusammengesetzt2= new LinkedBinaryTree(10, lbt2);
	assertTrue(zusammengesetzt.hashCode()==zusammengesetzt2.hashCode());
    }
    

}
